package com.example.a2lesson7;

import java.util.ArrayList;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity {
	private ListView diarylist;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btnadd = (Button) findViewById(R.id.btnadd);		
		diarylist = (ListView) findViewById(R.id.diary_list);
		btnadd.setOnClickListener(new OnClickListener(){		
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, AddActivity.class);
				startActivity(i);
			}
			
		});
		
		diarylist.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos,
					long id) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, ViewActivity.class);
				i.putExtra("diary", diarylist.getItemAtPosition(pos).toString());
				startActivity(i);
			}
			
		});
		getdiary();
	}
		
	
	private void getdiary()
	{
		ArrayList<String> diarystringlist = new ArrayList<String>();
		String url = "com.example.a2lesson7.DiaryContentProvider/diary";
		Uri diary = Uri.parse(url);
		Cursor c = managedQuery(diary, null, null, null, "");
		if (c.moveToFirst()) {
	         do{
	            diarystringlist.add(c.getString(1));
	         } while (c.moveToNext());
	      }
		
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this, R.layout.activity_main, diarystringlist);
		diarylist.setAdapter(aa);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
