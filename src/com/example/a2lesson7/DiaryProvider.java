package com.example.a2lesson7;

import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class DiaryProvider extends ContentProvider {

	static final String PROVIDER_NAME = "com.example.a2lesson7.DiaryProvider";
	static final String URL = "content://" + PROVIDER_NAME + "/diary";
	static final Uri CONTENT_URI = Uri.parse(URL);

	static final String _ID = "_id";
	static final String NAME = "name";
	static final String GRADE = "grade";

	private static HashMap<String, String> DIARY_PROJECTION_MAP;

	static final int ID = 1;
	static final int CONTENT = 2;
	
	

	static final UriMatcher uriMatcher;
	static{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "diary/#", ID);
		uriMatcher.addURI(PROVIDER_NAME, "diary", CONTENT);
	}
	
	private SQLiteDatabase db;		
	static final int DATABASE_VERSION = 1;
	static final String CREATE_DB_TABLE = 
			" CREATE TABLE diary" +
			" (_id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
			" content TEXT NOT NULL);";
	
	private static class DatabaseHelper extends SQLiteOpenHelper
	{
		
		

		public DatabaseHelper(Context context) {
			super(context, "diary", null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(CREATE_DB_TABLE);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion) {
			// TODO Auto-generated method stub
			 db.execSQL("DROP TABLE IF EXISTS diary");
	         onCreate(db);

		}

	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
	      int count = 0;

	      switch (uriMatcher.match(uri)){
	      case ID:
	         count = db.delete("diary", selection, selectionArgs);
	         break;
	      case CONTENT:
	         String id = uri.getPathSegments().get(1);
	         count = db.delete( "diary", ID +  " = " + id + 
	                (!TextUtils.isEmpty(selection) ? " AND (" + 
	                selection + ')' : ""), selectionArgs);
	         break;
	      default: 
	         throw new IllegalArgumentException("Unknown URI " + uri);
	      }
	      
	      getContext().getContentResolver().notifyChange(uri, null);
	      return count;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		switch (uriMatcher.match(uri)){
		/**
		 * Get all student records 
		 */
		case CONTENT:
			return "vnd.android.cursor.dir/vnd.example.diary";
			/** 
			 * Get a particular student
			 */
		case ID:
			return "vnd.android.cursor.item/vnd.example.diary";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}	
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		long rowid = db.insert("diary","", values);
		if (rowid >0)
		{
			Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowid);
			getContext().getContentResolver().notifyChange(_uri, null);
			return _uri;
		}
		throw new SQLException("Failed to add a record into " + uri);

	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		Context context = getContext();
		DatabaseHelper dbHelper = new DatabaseHelper(context);
		db = dbHelper.getWritableDatabase();
		return (db ==null)? false: true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,	
            String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		   SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		      qb.setTables("diary");
		      
		      switch (uriMatcher.match(uri)) {
		      case ID:
		    	 qb.appendWhere( _ID + "=" + uri.getPathSegments().get(1));		         
		         break;
		      case CONTENT:
		    	  qb.setProjectionMap(DIARY_PROJECTION_MAP);
		         break;
		      default:
		         throw new IllegalArgumentException("Unknown URI " + uri);
		      }
		      if (sortOrder == null || sortOrder == ""){
		         /** 
		          * By default sort on student names
		          */
		         sortOrder = NAME;
		      }
		      Cursor c = qb.query(db,	projection,	selection, selectionArgs, 
		                          null, null, sortOrder);
		      /** 
		       * register to watch a content URI for changes
		       */
		      c.setNotificationUri(getContext().getContentResolver(), uri);
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
	      int count = 0;
	      
	      switch (uriMatcher.match(uri)){
	      case CONTENT:
	         count = db.update("diary", values, 
	                 selection, selectionArgs);
	         break;
	      case ID:
	         count = db.update("diary", values, _ID + 
	                 " = " + uri.getPathSegments().get(1) + 
	                 (!TextUtils.isEmpty(selection) ? " AND (" +
	                 selection + ')' : ""), selectionArgs);
	         break;
	      default: 
	         throw new IllegalArgumentException("Unknown URI " + uri );
	      }
	      getContext().getContentResolver().notifyChange(uri, null);
	      return count;
	}

	

	

}
