package com.example.a2lesson7;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view);
		TextView diarytextview = (TextView) findViewById(R.id.diarytextview);
		Intent i = getIntent();
		String diarytext = i.getStringExtra("diary");
		diarytextview.setText(diarytext);
		
		Button btnback = (Button) findViewById(R.id.btnback);
		btnback.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ma = new Intent(ViewActivity.this, MainActivity.class);
				startActivity(ma);
			}
		
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view, menu);
		return true;
	}

}
