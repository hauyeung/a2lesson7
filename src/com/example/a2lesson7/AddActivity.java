package com.example.a2lesson7;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends Activity {

	private EditText diaryet;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
		Button btnadd = (Button) findViewById(R.id.btnadd);
		diaryet = (EditText) findViewById(R.id.diary);
		btnadd.setOnClickListener(new OnClickListener(){			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String diarytext = diaryet.getText().toString();
				adddiary(diarytext);
				Intent i = new Intent(AddActivity.this, MainActivity.class);
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add, menu);
		return true;
	}
	
	private void adddiary(String d)
	{
		ContentValues cv = new ContentValues();
		cv.put("content",d);
		getContentResolver().insert(DiaryProvider.CONTENT_URI,cv);
	}

}
